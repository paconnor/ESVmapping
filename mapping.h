#include <vector>
#include <map>

#include <TH1D.h>
#include <TH2D.h>

#include "variables.h"

using std::map;
using std::pair;
using std::vector;

class TDirectory;

struct Mapping {

    static map<ESV, int> GetESVsizes (const map<ESV, const vector<double>>& edges);
    static vector<double> GetBinIDedges (int N);

    const vector<double>& HTedges;
    const map<ESV, const vector<double>>& ESVedges;
    const map<ESV, int> nESVs;
    const int nHT, nESVtot;
    const vector<double> edges;
    const int N;

    Mapping (const vector<double>& HTbinning,
             const map<ESV, const vector<double>>& ESVbinning);

    int operator() (int iHT, ESV esv, double value) const;
    int operator() (double ht, ESV esv, double value) const;
    vector<int> operator() (double ht, const map<ESV,double>& esv) const;

    TH1D operator() (int iHT, ESV esv, const TH1D& hIn) const;
    TH1D operator() (double ht, ESV esv, const TH1D& hIn) const;
    TH2D operator() (pair<double,double> ht, pair<ESV,ESV> esv, const TH2D& covIn) const;
};

struct Dist {
    TH1D h;
    TH2D cov;
    const Mapping& mpg;

    Dist (const char* name, const char* title, const Mapping& mapping);

    void Fill (double ht, const map<ESV, double>& esv, double w);
    void Write (TDirectory * d);
};

struct RM {
    TH1D miss, fake;
    TH2D resp;
    const Mapping& mpgGen, mpgRec;

    RM (const char* name, const char* title, const Mapping& gen, const Mapping& rec);

    void FillRM (const pair<double          , double          >& ht,
                 const pair<map<ESV, double>, map<ESV, double>>& esv,
                 const pair<double          , double          >& w);
    void FillMiss (double ht, const map<ESV, double>& esv, double w);
    void FillFake (double ht, const map<ESV, double>& esv, double w);
    void Write (TDirectory * d);
};
