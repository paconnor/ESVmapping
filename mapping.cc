#include <cassert>

#include <iostream>
#include <numeric>
#include <algorithm>
#include <string>

#include <TMath.h>

#include "mapping.h"

using namespace std;

map<ESV, int> Mapping::GetESVsizes (const map<ESV, const vector<double>>& edges)
{
    map<ESV, int> nESVs;
    for (const auto& esv: edges) {
        int nESV = esv.second.size()-1;
        nESVs.insert( { esv.first, nESV } );
    }
    return nESVs;
}

vector<double> Mapping::GetBinIDedges (int N)
{
    vector<double> edges(N+1,0);
    for (int i = 0; i <= N; ++i)
        edges[i] = 0.5+i;
    return edges;
}

Mapping::Mapping (const vector<double>& HTbinning,
                  const map<ESV, const vector<double>>& ESVbinning) :
    HTedges(HTbinning),
    ESVedges(ESVbinning),
    nESVs(GetESVsizes(ESVedges)),
    nHT(HTedges.size()-1),
    nESVtot(accumulate(nESVs.begin(), nESVs.end(), 0, [](int sum, pair<ESV, int> nESV) { return sum + nESV.second;} )),
    edges(GetBinIDedges(nHT*nESVtot)),
    N(edges.size()-1)
{
    cout << __func__ << '\t' << N << '=' << nHT << "*("
         << accumulate(next(nESVs.begin()), nESVs.end(), to_string(nESVs.begin()->second),
                 [](string In, pair<ESV, int> nESV){ return In + '*' + to_string(nESV.second);} )
         << ')' << endl;
}

int Mapping::operator() (int iHT, ESV esv, double value) const
{
    const auto& edges = ESVedges.at(esv);
    const int& n = edges.size()-1;
    int offset = accumulate( nESVs.begin(), nESVs.find(esv), 0, [](int sum, pair<ESV, int> nESV) { return sum + nESV.second;} );
    int iESV = TMath::BinarySearch(n, edges.data(), value);
    int i = 1 + iHT + offset + iESV;
    return i;
}

int Mapping::operator() (double ht, ESV esv, double value) const
{
    int iHT = TMath::BinarySearch(nHT,HTedges.data(),ht);
    return operator()(iHT, esv, value);
}

vector<int> Mapping::operator() (double ht, const map<ESV,double>& esv) const
{
    vector<int> indices;
    indices.reserve(nHT);
    for (auto& v: esv) {
        int i = operator()(ht, v.first, v.second);
        indices.push_back(i);
    }
    return indices;
}

TH1D Mapping::operator() (int iHT, ESV esv, const TH1D& hIn) const
{
    auto name = Form("%s_esv%d_htbin%d", hIn.GetName(), esv, iHT);
    const auto& edges = ESVedges.at(esv);
    const int& n = edges.size()-1;
    TH1D hOut(name, "", n, edges.data());
    for (int i = 1; i <= n; ++i) {
        auto center = hOut.GetBinCenter(i);
        auto binID = operator()(iHT, esv, center);
        auto content = hIn.GetBinContent(binID);
        hOut.SetBinContent(i, content);
    }
    return hOut;
}

TH1D Mapping::operator() (double ht, ESV esv, const TH1D& hIn) const
{
    int iHT = TMath::BinarySearch(nHT,HTedges.data(),ht);
    return operator()(iHT, esv, hIn);
}

TH2D Mapping::operator() (pair<double,double> ht, pair<ESV,ESV> esv, const TH2D& covIn) const
{
    int iHT1 = TMath::BinarySearch(nHT,HTedges.data(),ht.first ),
        iHT2 = TMath::BinarySearch(nHT,HTedges.data(),ht.second);
    ESV esv1 = esv.first, esv2 = esv.second;
    auto name = Form("%s_esv%d_esv%d_htbin%d_htbin%d", covIn.GetName(), esv1, esv2, iHT1, iHT2);
    const auto& edges1 = ESVedges.at(esv1), edges2 = ESVedges.at(esv2);
    const int& n1 = edges1.size()-1, n2 = edges2.size()-1;
    TH2D covOut(name, "", n1, edges1.data(), n2, edges2.data());
    for (int i1 = 1; i1 <= n1; ++i1) 
        for (int i2 = 1; i2 <= n2; ++i2) {
            auto center1 = covOut.GetXaxis()->GetBinCenter(i1),
                 center2 = covOut.GetYaxis()->GetBinCenter(i2);
            auto binID1 = operator()(iHT1, esv1, center1),
                 binID2 = operator()(iHT2, esv2, center2);
            auto content = covIn.GetBinContent(binID1, binID2);
            covOut.SetBinContent(i1, i2, content);
        }
    return covOut;
}

Dist::Dist (const char* name, const char* title, const Mapping& mapping) :
    h  (Form(  "h_%s", name), title, mapping.N, mapping.edges.data()),
    cov(Form("cov_%s", name), title, mapping.N, mapping.edges.data(), mapping.N, mapping.edges.data()),
    mpg(mapping)
{
    cout << __func__ << '\t' << name << endl;
}

void Dist::Fill (double ht, const map<ESV, double>& esv, double w)
{
    auto binIDs = mpg(ht, esv);

    for (auto i: binIDs) {
        h.Fill(i, w);
        for (auto j: binIDs)
            cov.Fill(i,j,w*w);
    }
}

void Dist::Write (TDirectory * d)
{
    h.SetDirectory(d);
    h.Write();
    cov.SetDirectory(d);
    cov.Write();
}

RM::RM (const char* name, const char* title, const Mapping& gen, const Mapping& rec) :
    miss(Form("miss_%s", name), title, gen.N, gen.edges.data()),
    fake(Form("fake_%s", name), title,                          rec.N, rec.edges.data()),
    resp(Form(  "RM_%s", name), title, gen.N, gen.edges.data(), rec.N, rec.edges.data()),
    mpgGen(gen), mpgRec(rec)
{
    cout << __func__ << '\t' << name << endl;
}

void RM::FillRM (const pair<double          , double          >& ht,
                 const pair<map<ESV, double>, map<ESV, double>>& esv,
                 const pair<double          , double          >& w)
{
    auto iGens = mpgGen(ht.first , esv.first ),
         iRecs = mpgRec(ht.second, esv.second);
    assert(iGens.size() == iRecs.size());

    for (size_t i = 0; i < iGens.size(); ++i) {
        auto iGen = iGens.at(i), iRec = iRecs.at(i);
        resp.Fill(iGen, iRec, w.first *    w.second );
        miss.Fill(iGen      , w.first * (1-w.second));
    }
}

void RM::FillMiss (double ht, const map<ESV, double>& esv, double w)
{
    auto binIDs = mpgGen(ht, esv);
    for (auto i: binIDs) miss.Fill(i, w);
}

void RM::FillFake (double ht, const map<ESV, double>& esv, double w)
{
    auto binIDs = mpgRec(ht, esv);
    for (auto i: binIDs) fake.Fill(i, w);
}

void RM::Write (TDirectory * d)
{
    miss.SetDirectory(d);
    miss.Write();
    fake.SetDirectory(d);
    fake.Write();
    resp.SetDirectory(d);
    resp.Write();
}

