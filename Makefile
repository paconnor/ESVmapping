ROOTINC    = $(shell root-config --incdir)
ROOTCFLAGS = $(shell root-config --cflags)
ROOTLIBS   = $(shell root-config --libs)

CFLAGS=-g -Wall -O3 -std=c++17 -fPIC -DDEBUG

all: libmapping.so example
	./example

%: %.cc
	g++ $(CFLAGS) $(ROOTCFLAGS) $^ $(ROOTLIBS) -lUnfold -lXMLIO -lXMLParser -o $@ -L. -Wl,-rpath,. -lrt -lmapping

lib%.so: %.h %.cc 
	g++ ${CFLAGS} $(ROOTCFLAGS) -shared $^ $(ROOTLIBS) -o $@

clean: 
	@rm -f *.pdf *.root

.PHONY: all clean
