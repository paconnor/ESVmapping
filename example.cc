#include "mapping.h"

#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TFile.h>

#include <cstdlib>
#include <iostream>

using namespace std;

int main (int argc, char * argv[])
{ 
    cout << "Starting" << endl;

    cout << thrust_gen.size() << ' ' << rho_gen.size() << '\n'        
         << thrust_reco.size() << ' ' << rho_reco.size() << endl;

    const map<ESV, const vector<double>> ESVbinsGen = {{ ESV::Thrust, thrust_gen }, { ESV::RhoMass, rho_gen }},
                                         ESVbinsRec = {{ ESV::Thrust, thrust_reco}, { ESV::RhoMass, rho_reco}};

    const Mapping gen(HTbins, ESVbinsGen),
                  rec(HTbins, ESVbinsRec);

    Dist distGen("nominalGen", "nominal variation at particle level", gen),
         distRec("nominalRec", "nominal variation at detector level", rec);

    RM rm("nominal", "nominal variation", gen, rec);

    cout << "Example of event" << endl;
    // TODO: put this logic in the event loop and change hardcoded values by values from n-tuple
    {
        auto genHT = 160., recHT = 150.;

        auto genThrust = -4.8, recThrust = -4.1;
        auto genRhoMass = -1.2, recRhoMass = -1.3;
        const map<ESV, double> genESV {{ ESV::Thrust, genThrust }, { ESV::RhoMass, genRhoMass }},
                               recESV {{ ESV::Thrust, recThrust }, { ESV::RhoMass, recRhoMass }};

        auto genWgt = 1.0, recWgt = 1.1;

        distGen.Fill(genHT, genESV, genWgt);
        distRec.Fill(recHT, recESV, recWgt);
        rm.FillRM({genHT, recHT}, {genESV, recESV}, {genWgt, recWgt});
        // TODO: use rm.FillFake() and rm.FillMiss() if bad event selection or out of range
    }

    cout << "Writing" << endl;
    {
        auto f = TFile::Open("out.root", "RECREATE");
        distGen.Write(f);
        distRec.Write(f);
        rm.Write(f);
        f->Close();
    }

    cout << "Example of projection" << endl;
    {
        gStyle->SetOptStat(0);
        TCanvas c("example", "example");

        // h
        distGen.h.SetTitle("example of flattened multi-dimensional histogram;bin ID;N_{eff}");
        distGen.h.Draw();
        c.Print("out.pdf(");
        auto hGenTest = gen(155., ESV::Thrust, distGen.h);
        hGenTest.SetTitle("example of histogram with physical axis;Thrust;N_{eff}");
        hGenTest.Draw();
        c.Print("out.pdf");
        hGenTest = gen(155., ESV::RhoMass, distGen.h);
        hGenTest.SetTitle("example of histogram with physical axis;RhoMass;N_{eff}");
        hGenTest.Draw();
        c.Print("out.pdf");

        // cov
        distGen.cov.SetTitle("example of flattened multi-dimensional covariance matrix;bin ID;N_{eff}");
        distGen.cov.Draw("colz");
        c.Print("out.pdf");
        auto covGenTest = gen({155.,155.}, {ESV::Thrust, ESV::Thrust}, distGen.cov);
        covGenTest.SetTitle("example of covariance between two ESVs;Thrust;Thrust");
        covGenTest.Draw("colz");
        c.Print("out.pdf");
        covGenTest = gen({155.,155.}, {ESV::RhoMass, ESV::RhoMass}, distGen.cov);
        covGenTest.SetTitle("example of covariance between two ESVs;RhoMass;RhoMass");
        covGenTest.Draw("colz");
        c.Print("out.pdf");
        covGenTest = gen({155.,155.}, {ESV::Thrust, ESV::RhoMass}, distGen.cov);
        covGenTest.SetTitle("example of covariance between two ESVs;Thrust;RhoMass");
        covGenTest.Draw("colz");
        c.Print("out.pdf)");
    }

    cout << "Done" << endl;
    return EXIT_SUCCESS;
}

